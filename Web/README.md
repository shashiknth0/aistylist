### :rocket: **Progressive Web App:** [Lighthouse](https://github.com/GoogleChrome/lighthouse) score of __95/100__.


## What's included?

Currently, this is a fairly basic vesion of the application. We are calling it a pre-alpha release.

### What's working and ready to be deployed?
* Add/Remove products to cart.
* Select/Clear filters based on category.
* Support for product variants.
* Cart checkout feature.
* Cash on delivery option.
* Authentication (Login/Signup)

### What's coming very soon?
* __AI Personal Fit__


## What in the Tech News?


As of now, the application has 7 major modules, `products`, `core`, `home`, `user`, `checkout`, `auth`, `shared`.

We are working on documentation and we can share that once we are looking at a more stable release.

* Exclusively using @ngrx libraries(store, effects, actions), showcasing common patterns and best practices.
* Fully Observable approach using RxJS 5.0.1(latest beta).
* Uses @ngrx/store to manage the state of the app and to cache requests made to the Backend API, 
* @angular/router to manage navigation between routes, 
* @ngrx/effects to isolate side effects.
* @ngrx/actions to define the actions on the frontend.
* Following Container/Presentation component approach.
* Lazy loading of modules(for modules which are not immediately required for first painting the DOM).
* ImmutableJs to create and safeguard objects againts mutability.
* Project is divided into modules which are more or less independant of each other except core module.

## Screenshots

### Home Page

On this page user can filter products as per category. Change layout of the products(cozy, comfortable) etc.


### Cart Page

Cart page displays all the line items or items in the cart which the user has added while browsing the website.


### Product detail page

Display's the detailed product information of a particular product.




### Package Manager [Yarn](https://yarnpkg.com/en/)

We are using Yarn as a package manager in this project though you can also use `npm` if you like to.

## Backend for this project?


**Start the API for this project to work successfully.


## Contributing

Where to start

There are many different ways to contribute to development, just find the one that best fits with your skills. Examples of contributions we would love to receive include:

* Code patches
* Documentation improvements
* Translations(yet to come)
* Bug reports
* Patch reviews
* UI enhancements


## Development server & Mock Api
Run `npm start-mock` for a dev server with 'mock' api. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files and will fetch data from mock api;

## Development server
Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Development server with Service Worker
Run `npm run build--prod` to build in production with service worker pre-cache and then `npm run static-serve` to serve.

## Build with Service Worker

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `npm run build--prod` for a production build.

